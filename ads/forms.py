from django import forms
from ads.models import Ads, type_building, rooms_quantity, heat_type, type_ads


class PostCreateForm(forms.ModelForm):
    ads_type = forms.ChoiceField(choices=type_ads, label='Тип Недвижимости',
                                 required=True, widget=forms.Select(
                                            attrs={'class': 'my-select form-control'}
                                            ))
    ads_title = forms.CharField(label='Заголовок Обьявления', required=True,
                                widget=forms.TextInput(
                                    attrs={'class': 'form-control'}))
    ads_address = forms.CharField(label='Район недвижимости', required=True,
                                  widget=forms.TextInput(
                                      attrs={'class': 'form-control'}))
    ads_phone = forms.CharField(label='Телефон', required=True,
                                widget=forms.TextInput(
                                    attrs={'class': 'form-control'}))
    ads_body = forms.CharField(label='Описание Обьявления', required=True,
                               widget=forms.Textarea(
                                   attrs={'class': 'form-control'}))
    ads_rooms = forms.ChoiceField(choices=rooms_quantity, initial=' ', label='Кол-во комнат', required=True,
                                  widget=forms.Select(
                                      attrs={'class': 'my-select form-control'}
                                  ))
    ads_square = forms.CharField(label='Общая площадь', required=True,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control'}
                                 ))
    ads_heat = forms.ChoiceField(choices=heat_type, initial=' ', label='Тип отопления', required=True,
                                 widget=forms.Select(
                                     attrs={'class': 'my-select form-control'}
                                 ))
    ads_floor = forms.CharField(label='Этаж', widget=forms.TextInput(
        attrs={'class': 'form-control'}
    ))
    ads_type_building = forms.ChoiceField(choices=type_building, initial=' ', label='Тип здания',
                                          required=True, widget=forms.Select(
                                            attrs={'class': 'my-select form-control'}
                                            ))
    ads_price = forms.CharField(label='Цена', required=True,
                                widget=forms.TextInput(
                                    attrs={'class': 'form-control'}
                                ))
    ads_image1 = forms.ImageField(label='Изображения', required=True,
                                  widget=forms.FileInput(
                                      attrs={'class': 'form-control',
                                             'onchange': 'myFunction2()'}
                                  ))
    ads_image2 = forms.ImageField(label='', required=False,
                                  widget=forms.FileInput(
                                      attrs={'class': 'form-control',
                                             'style': 'display:none',
                                             'onchange': 'myFunction3()'}
                                  ))
    ads_image3 = forms.ImageField(label='', required=False,
                                  widget=forms.FileInput(
                                      attrs={'class': 'form-control',
                                             'style': 'display:none',
                                             'onchange': 'myFunction4()'}
                                  ))
    ads_image4 = forms.ImageField(label='', required=False,
                                  widget=forms.FileInput(
                                      attrs={'class': 'form-control',
                                             'style': 'display:none',
                                             'onchange': 'myFunction5()'}
                                  ))
    ads_image5 = forms.ImageField(label='', required=False,
                                  widget=forms.FileInput(
                                      attrs={'class': 'form-control',
                                             'style': 'display:none',
                                             'onchange': 'myFunction6()'}
                                  ))
    ads_image6 = forms.ImageField(label='', required=False,
                                  widget=forms.FileInput(
                                      attrs={'class': 'form-control',
                                             'style': 'display:none',
                                             'onchange': 'myFunction7()'}
                                  ))
    ads_image7 = forms.ImageField(label='', required=False,
                                  widget=forms.FileInput(
                                      attrs={'class': 'form-control',
                                             'style': 'display:none',
                                             'onchange': 'myFunction8()'}
                                  ))
    ads_image8 = forms.ImageField(label='', required=False,
                                  widget=forms.FileInput(
                                      attrs={'class': 'form-control',
                                             'style': 'display:none',
                                             'onchange': 'myFunction9()'}
                                  ))
    ads_image9 = forms.ImageField(label='', required=False,
                                  widget=forms.FileInput(
                                      attrs={'class': 'form-control',
                                             'style': 'display:none',
                                             'onchange': 'myFunction10()'}
                                  ))
    ads_image10 = forms.ImageField(label='', required=False,
                                   widget=forms.FileInput(
                                       attrs={'class': 'form-control',
                                              'style': 'display:none'}
                                   ))

    class Meta:
        model = Ads
        fields = ['ads_type', 'ads_title', 'ads_phone', 'ads_address', 'ads_rooms', 'ads_square', 'ads_floor',
                  'ads_type_building', 'ads_heat', 'ads_price', 'ads_image1',
                  'ads_image2', 'ads_image3', 'ads_image4',
                  'ads_image5', 'ads_image6', 'ads_image7',
                  'ads_image8', 'ads_image9', 'ads_image10', 'ads_body']

    # def __init__(self, *args, **kwargs):
    #     super(PostCreateForm, self).__init__(*args, **kwargs)
    #     self.fields["ads_type_building"].choices = [("", "  "), ] + \
    #                                       list(self.fields["ads_type_building"].choices)[1:]
