from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from ads.models import Ads


class AdsAdmin(ImportExportModelAdmin):
    list_display = ('ads_title', 'ads_phone', 'ads_address', )
    list_display_links = ('ads_title', 'ads_phone', 'ads_address', )

admin.site.register(Ads, AdsAdmin)