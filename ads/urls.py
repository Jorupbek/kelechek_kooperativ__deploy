from django.urls import path
from ads.views import (AdsCreateView, AdsListView,
                       AdsDetailView, AdsDeleteView, AdsUpdateView, AdsTableView, make_hot, up_ad, AdsUncheckedListView,
                       change_status)

urlpatterns = [
    path('ads_list/', AdsListView.as_view(), name='ads_list'),
    path('ads_unchecked/', AdsUncheckedListView.as_view(), name='ads_unchecked'),
    path('ads_table/', AdsTableView.as_view(), name='ads_table'),
    path('ads_create/', AdsCreateView.as_view(), name='ads_create'),
    path('ads_detail/<int:pk>/', AdsDetailView.as_view(), name='ads_detail'),
    path('ads_delete/<int:pk>/', AdsDeleteView.as_view(), name='ads_delete'),
    path('ads_update/<int:pk>/', AdsUpdateView.as_view(), name='ads_update'),
    path('make_hot/<int:pk>/', make_hot, name='make_hot'),
    path('up_ad/<int:pk>/', up_ad, name='up_ad'),
    path('change_status/<int:pk>/', change_status, name='change_status'),
]
