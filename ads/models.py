from django.db import models
from _datetime import datetime
from django.utils import timezone

type_building = [
    ('a', ' '),
    ('Новостройка', 'Новостройка'),
    ('Вторичное жилье', 'Вторичное жилье'),
]
rooms_quantity = [
    ('a', ' '),
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5+', '5+'),
]
heat_type = [
    ('a', ' '),
    ('Центральное', 'Центральное'),
    ('Паровое', 'Паровое'),
    ('Электрическое', 'Электрическое'),
    ('Газовое', 'Газовое'),
    ('Электрическое + Паровое', 'Электрическое + Паровое'),
    ('Газовое + Паровое', 'Газовое + Паровое'),
    ('Газовое + Электрическое', 'Газовое + Электрическое'),
    ('Газовое + Электрическое + Паровое', 'Газовое + Электрическое + Паровое'),
    ('Другое', 'Другое'),
]
type_ads = [
    ('Продать Квартиру', 'Продать Квартиру'),
    ('Продать Дом', 'Продать Дом'),
    ('Продать Участок', 'Продать Участок'),
    ('Продать Бизнес', 'Продать Под Бизнес'),
    ('Продать Дом/Участок', 'Продать Дом/Участок'),
    ('Продать Офис', 'Продать Офис'),
    ('Снять Квартиру', 'Снять Квартиру'),
    ('Снять Дом', 'Снять Дом'),
    ('Снять Участок', 'Снять Участок'),
    ('Снять Бизнес', 'Снять Под Бизнес'),
    ('Снять Дом/Участок', 'Снять Дом/Участок'),
    ('Снять Офис', 'Снять Офис'),
]


class Ads(models.Model):
    ads_type = models.CharField(max_length=100, choices=type_ads, verbose_name="Тип недвижимости")
    ads_title = models.CharField(max_length=255, verbose_name="Заголовок")
    ads_phone = models.CharField(max_length=50, verbose_name="Телефон")
    ads_address = models.CharField(max_length=150, verbose_name="Адрес")
    ads_body = models.TextField(verbose_name="Краткое описание")
    ads_rooms = models.CharField(max_length=5, choices=rooms_quantity, verbose_name="КОл-во комнат")
    ads_square = models.IntegerField(verbose_name="Площадь")
    ads_heat = models.CharField(max_length=150, choices=heat_type, verbose_name="Отопление")
    ads_floor = models.IntegerField(blank=True, null=True, verbose_name="Этаж")
    ads_type_building = models.CharField(max_length=30, choices=type_building, verbose_name="Тип здания")
    ads_price = models.CharField(max_length=30, verbose_name="Цена")
    ads_date = models.DateTimeField(default=datetime.now, verbose_name="Дата публикации")
    ads_update_date = models.DateTimeField(auto_now=True, verbose_name="Дата поднятия")
    ads_hot = models.BooleanField(default=False, verbose_name="Срочное")
    ads_status = models.BooleanField(default=False, verbose_name="Опубликовать")
    ads_image1 = models.ImageField(upload_to='images', blank=True, null=True, verbose_name="Изображение")
    ads_image2 = models.ImageField(upload_to='images', blank=True, null=True, verbose_name="Изображение")
    ads_image3 = models.ImageField(upload_to='images', blank=True, null=True, verbose_name="Изображение")
    ads_image4 = models.ImageField(upload_to='images', blank=True, null=True, verbose_name="Изображение")
    ads_image5 = models.ImageField(upload_to='images', blank=True, null=True, verbose_name="Изображение")
    ads_image6 = models.ImageField(upload_to='images', blank=True, null=True, verbose_name="Изображение")
    ads_image7 = models.ImageField(upload_to='images', blank=True, null=True, verbose_name="Изображение")
    ads_image8 = models.ImageField(upload_to='images', blank=True, null=True, verbose_name="Изображение")
    ads_image9 = models.ImageField(upload_to='images', blank=True, null=True, verbose_name="Изображение")
    ads_image10 = models.ImageField(upload_to='images', blank=True, null=True, verbose_name="Изображение")

    def __str__(self):
        return self.ads_title
