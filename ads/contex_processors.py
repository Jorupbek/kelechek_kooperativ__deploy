from .models import Ads


def ads_context(request):
    published_ads = Ads.objects.order_by('-id')
    return {'ads_context': published_ads}


def last_ads_context(request):
    images_array = Ads.objects.order_by('-id')[:4]
    return {'last_ads_context': images_array}


def main_ads_context(request):
    ads2_context = Ads.objects.order_by('-id')[:7]
    return {'main_ads_context': ads2_context}
