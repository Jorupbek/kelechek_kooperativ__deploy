from _datetime import datetime

from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import redirect
from django.template.defaulttags import register
from django.urls import reverse_lazy
from django.views.generic import (CreateView, ListView, DetailView, DeleteView,
                                  UpdateView)

from ads.forms import PostCreateForm
from ads.models import Ads
from django.http import HttpResponseRedirect


class AdsCreateView(CreateView):
    model = Ads
    form_class = PostCreateForm
    template_name = 'ads/ads_create.html'
    success_url = reverse_lazy('home')


class AdsListView(UserPassesTestMixin, ListView):
    model = Ads
    template_name = 'ads/ads_list.html'
    queryset = Ads.objects.filter(ads_status=True).order_by('-id')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator


class AdsUncheckedListView(UserPassesTestMixin, ListView):
    model = Ads
    template_name = 'ads/ads_list.html'
    queryset = Ads.objects.filter(ads_status=False).order_by('-id')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator


class AdsTableView(ListView):
    model = Ads
    template_name = 'ads/ads_table.html'
    paginate_by = 50
    queryset = Ads.objects.filter(ads_status=True).order_by('-ads_hot', '-ads_update_date')


class AdsDetailView(DetailView):
    model = Ads
    template_name = 'ads/ads_detail.html'


class AdsDeleteView(UserPassesTestMixin, DeleteView):
    model = Ads
    template_name = 'ads/ads_delete.html'
    success_url = reverse_lazy('ads_list')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator


class AdsUpdateView(UserPassesTestMixin, UpdateView):
    model = Ads
    template_name = 'ads/ads_update.html'
    fields = ['ads_type', 'ads_title', 'ads_address', 'ads_body', 'ads_rooms', 'ads_square', 'ads_floor',
              'ads_type_building', 'ads_price', 'ads_image1',
              'ads_image2', 'ads_image3', 'ads_image4',
              'ads_image5', 'ads_image6', 'ads_image7',
              'ads_image8', 'ads_image9', 'ads_image10',
              'ads_status']
    success_url = reverse_lazy('ads_list')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator


@register.filter
def get_range(value):
    return range(value+1)


def make_hot(request, pk):
    move = Ads.objects.get(pk=pk)
    if move.ads_hot:
        move.ads_hot = False
    else:
        move.ads_hot = True
    move.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def up_ad(request, pk):
    up = Ads.objects.get(pk=pk)
    up.ads_update_date = datetime.now
    up.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def change_status(request, pk):
    status = Ads.objects.get(pk=pk)
    if not status.ads_status:
        status.ads_status = True
    else:
        status.ads_status = False
    status.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
