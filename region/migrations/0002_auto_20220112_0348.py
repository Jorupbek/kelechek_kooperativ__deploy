# Generated by Django 3.0 on 2022-01-11 21:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('region', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(
            """
                INSERT INTO region_userregion 
                SELECT * FROM users_customuserregion;
                
                SELECT setval(pg_get_serial_sequence('"region_userregion"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "region_userregion";
            """,
            reverse_sql="""
                INSERT INTO users_customuserregion 
                SELECT * FROM region_userregion;
            """)
    ]
