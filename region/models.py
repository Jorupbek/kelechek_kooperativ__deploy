from django.db import models


class UserRegion(models.Model):
    region_name = models.CharField(verbose_name='Регион',
                                   max_length=255, unique=True)
    region_title = models.TextField(verbose_name="Название ОсОО",
                                    null=True, blank=True)
    region_address = models.TextField(verbose_name="Адрес региона",
                                      null=True, blank=True)
    region_inn = models.CharField(verbose_name="ИНН региона", max_length=255,
                                  null=True, blank=True)
    region_okpo = models.CharField(verbose_name="ОКПО региона", max_length=255,
                                   null=True, blank=True)
    region_phone = models.CharField(verbose_name="Телефон региона",
                                    max_length=255,
                                    null=True, blank=True)
    region_director = models.CharField(verbose_name="Директор региона",
                                       max_length=255,
                                       null=True, blank=True)
    region_bank = models.CharField(verbose_name="Название банка",
                                   max_length=255, null=True, blank=True)
    region_bank_iid = models.CharField(verbose_name="Рассчетный счет",
                                       max_length=255, null=True, blank=True)
    region_bank_bik = models.CharField(verbose_name="БИК Банка",
                                       max_length=255, null=True, blank=True)

    def __str__(self):
        return self.region_name
