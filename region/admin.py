from django.contrib import admin
from region.models import UserRegion


@admin.register(UserRegion)
class CustomUserRegionAdmin(admin.ModelAdmin):
    list_display = ('region_name',)
    list_display_links = ('region_name',)
