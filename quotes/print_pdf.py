from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.template.loader import render_to_string

from weasyprint import HTML

from quotes.models import Question


def generate_pdf(request, question_pk):
    question = Question.objects.get(pk=question_pk)

    html_string = render_to_string('votes/pdf_template.html', {'question':
                                                               question})

    html = HTML(string=html_string)
    html.write_pdf(target=f'/tmp/{question.question_date}.pdf')

    fs = FileSystemStorage('/tmp')
    with fs.open(f'{question.question_date}.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = f'attachment; ' \
                                          f'filename="{question.question_date}.pdf"'
        return response
