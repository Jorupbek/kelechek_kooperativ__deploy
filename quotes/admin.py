from django.contrib import admin
from quotes.models import Question, Choice


class ChoiceInLine(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    list_display = ['question_number', 'pub_date', 'question_text_1']
    list_display_links = ['question_number', 'pub_date', 'question_text_1']
    inlines = [ChoiceInLine]
    filter_horizontal = ["users"]


admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice)
