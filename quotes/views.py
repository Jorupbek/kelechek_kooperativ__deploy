from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.db import transaction
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView

from quotes.forms import CreationForm, CreationTitleFormSet, UpdateForm
from quotes.models import Question, Choice


def votes_list(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'question_list': latest_question_list}
    return render(request, 'votes/votes_list.html', context)


def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'votes/detail.html', {'question': question})


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'votes/results.html', {'question': question})


def vote(request, question_id):
    question = (Question.objects
                .filter(pk=question_id, is_active=True)
                .prefetch_related('choice_set', 'users').first())
    if not question:
        return HttpResponseRedirect(
            reverse('quotes:votes'))
    count = question.users.all().count
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
        if request.user in question.users.all():
            messages.success(request, "Вы уже голосовали")
            return HttpResponseRedirect(
                reverse('user_detail', args=(request.user.id,)))
        if not (request.user.is_superuser or request.user.user_is_operator or
                request.user.user_is_queued or request.user.is_moderator):
            question.users.add(request.user)
        else:
            question.users.add(request.user)
    except (KeyError, Choice.DoesNotExist):
        messages.success(request, "Вы не выбрали")
        return render(request, 'votes/vote_detail.html', {
            'question': question, 'users_count': count
        })
    else:
        selected_choice.votes += 1
        selected_choice.user.add(request.user)
        selected_choice.save()
        return HttpResponseRedirect(
            reverse('quotes:vote', args=(question.id,)))


class VoteCreateView(UserPassesTestMixin, CreateView):
    model = Question
    template_name = 'votes/create.html'
    form_class = CreationForm
    success_url = None

    def get_context_data(self, **kwargs):
        data = super(VoteCreateView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['titles'] = CreationTitleFormSet(self.request.POST)
        else:
            data['titles'] = CreationTitleFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        titles = context['titles']
        with transaction.atomic():
            form.instance.created_by = self.request.user
            self.object = form.save()
            if titles.is_valid():
                titles.instance = self.object
                titles.save()
        return super(VoteCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('quotes:votes')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class VoteUpdateView(UserPassesTestMixin, UpdateView):
    model = Question
    template_name = 'votes/update.html'
    form_class = UpdateForm

    def get_context_data(self, **kwargs):
        data = super(VoteUpdateView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['titles'] = CreationTitleFormSet(self.request.POST)
        else:
            data['titles'] = CreationTitleFormSet()
        return data

    def get_success_url(self):
        return reverse_lazy('quotes:votes')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class VoteDeleteView(UserPassesTestMixin, DeleteView):
    model = Question
    template_name = 'votes/delete.html'
    success_url = reverse_lazy('quotes:votes')

    def test_func(self):
        return self.request.user.is_superuser
