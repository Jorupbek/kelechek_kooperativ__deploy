from django import forms
from django.forms.models import inlineformset_factory
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Div

from quotes.custom_layout_object import Formset
from quotes.models import Question, Choice


class CcreationTitleForm(forms.ModelForm):

    class Meta:
        model = Choice
        exclude = ()


CreationTitleFormSet = inlineformset_factory(
    Question, Choice, form=CcreationTitleForm,
    fields=['choice_text'], extra=3, can_delete=False
    )


class CreationForm(forms.ModelForm):

    class Meta:
        model = Question
        exclude = ['created_by', ]

    def __init__(self, *args, **kwargs):
        super(CreationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(
                Fieldset('Варианты ответов', Formset('titles')),
            ))


class UpdateForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = ['question_number', 'question_name', 'question_adr',
                  'question_city', 'question_date', 'question_text_1',
                  'question_text_2', 'question_text_3', 
                  'question_file', 'is_active']
