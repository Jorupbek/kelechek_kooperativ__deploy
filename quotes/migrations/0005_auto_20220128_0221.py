# Generated by Django 3.0 on 2022-01-27 20:21

from django.conf import settings
from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('quotes', '0004_auto_20220118_0039'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='file',
        ),
        migrations.RemoveField(
            model_name='question',
            name='is_active',
        ),
        migrations.AddField(
            model_name='choice',
            name='user',
            field=models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='question',
            name='question_adr',
            field=models.CharField(blank=True, help_text='г.Бишкек , ул. Севастопольская д.12', max_length=255, null=True, verbose_name='Место проведения'),
        ),
        migrations.AddField(
            model_name='question',
            name='question_text_3',
            field=tinymce.models.HTMLField(blank=True, null=True, verbose_name='ЕДИНОГЛАСНО РЕШИЛИ'),
        ),
        migrations.AlterField(
            model_name='choice',
            name='choice_text',
            field=models.CharField(help_text='Да или Нет', max_length=200, verbose_name='Название ответа'),
        ),
        migrations.AlterField(
            model_name='question',
            name='question_city',
            field=models.CharField(blank=True, help_text='г. Бишкек', max_length=255, null=True, verbose_name='Название города'),
        ),
        migrations.AlterField(
            model_name='question',
            name='question_name',
            field=models.CharField(help_text='Короткое название вопроса', max_length=255, verbose_name='Название вопроса'),
        ),
        migrations.AlterField(
            model_name='question',
            name='question_number',
            field=models.IntegerField(default=0, help_text='Вписывается номер протокола №1', verbose_name='№ Протокола'),
        ),
        migrations.AlterField(
            model_name='question',
            name='question_text_1',
            field=tinymce.models.HTMLField(blank=True, null=True, verbose_name='ПОВЕСТКА ДНЯ'),
        ),
        migrations.AlterField(
            model_name='question',
            name='question_text_2',
            field=tinymce.models.HTMLField(blank=True, null=True, verbose_name='ПРОТОКОЛ'),
        ),
    ]
