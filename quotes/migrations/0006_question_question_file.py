# Generated by Django 3.2.5 on 2023-08-07 04:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quotes', '0005_auto_20220128_0221'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='question_file',
            field=models.FileField(blank=True, null=True, upload_to='vote', verbose_name='Документ протоколоа'),
        ),
    ]
