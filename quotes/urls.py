from django.urls import path

from quotes.views import (
    votes_list, results, vote, detail, VoteCreateView,
    VoteUpdateView, VoteDeleteView
)

app_name = 'quotes'
urlpatterns = [
    path('', votes_list, name='votes'),
    path('<int:question_id>/', detail, name='detail'),
    path('<int:question_id>/results/', results, name='results'),
    path('<int:question_id>/vote/', vote, name='vote'),
    path('create/', VoteCreateView.as_view(), name='vote_create'),
    path('update/<int:pk>', VoteUpdateView.as_view(),
         name='vote_update'),
    path('delete/<int:pk>', VoteDeleteView.as_view(),
         name='vote_delete'),
]
