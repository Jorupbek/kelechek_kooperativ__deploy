from django.db import models
from tinymce.models import HTMLField

from users.models import CustomUser


class Question(models.Model):
    question_number = models.IntegerField("№ Протокола", default=0,
                                help_text="Вписывается номер протокола №1")
    question_name = models.CharField('Название вопроса', max_length=255,
                                     help_text="Короткое название вопроса")
    question_adr = models.CharField("Место проведения", null=True,
                                    blank=True, max_length=255,
                                    help_text="г.Бишкек , ул. Севастопольская д.12")
    question_city = models.CharField("Название города", help_text="г. Бишкек",
                                     max_length=255, null=True, blank=True,)
    question_date = models.DateField('Дата протокола', null=True, blank=True)
    pub_date = models.DateTimeField('Дата публикации', auto_now_add=True)
    question_text_1 = HTMLField("ПОВЕСТКА ДНЯ",
                                       null=True, blank=True)
    question_text_2 = HTMLField("ПРОТОКОЛ",
                                       null=True, blank=True)
    question_text_3 = HTMLField("ЕДИНОГЛАСНО РЕШИЛИ",
                                       null=True, blank=True)
    question_file = models.FileField('Документ протоколоа', null=True, blank=True, 
                                     upload_to='vote')
    is_active = models.BooleanField(default=True, verbose_name='Активный вопрос')
    users = models.ManyToManyField(CustomUser, blank=True)

    def __str__(self):
        return f'{self.question_number}'


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField("Название ответа", help_text="Да или Нет",
                                   max_length=200)
    votes = models.IntegerField(default=0)
    user = models.ManyToManyField(CustomUser, blank=True)

    def __str__(self):
        return self.choice_text
