from django.urls import path
from django.contrib.auth import views
from users.views import (UserListView, UserGivenView, UserDetail,
                         UserUpdateView, UserDeleteView,
                         user_to_delete_list, SignUpView, login_success,
                         AgreementListView, Agreement1, Agreement2,
                         Agreement3, Agreement4, Agreement5, Agreement_1,
                         Agreement_2, AgreementPrilojenie3,
                         AgreementCreateView, Agreement1UpdateView,
                         Agreement2UpdateView, Agreement3UpdateView,
                         Agreement4UpdateView, Agreement5UpdateView,
                         Agreement_1UpdateView, OperatorSignUp,
                         OperatorsList, user_password_reset, NotificationList,
                         NotificationCreate, NotificationDelete)

urlpatterns = [
    path('users/', UserListView.as_view(), name='users_list'),
    path('users_given_list/', UserGivenView.as_view(), name='users_given_list'),
    path('user/<int:pk>', UserDetail.as_view(), name='user_detail'),
    path('user/<int:pk>/update', UserUpdateView.as_view(), name='user_update'),
    path('user/<int:pk>/delete', UserDeleteView.as_view(), name='user_delete'),
    path('delete/<int:pk>/', user_to_delete_list, name='delete'),
    path('user/create', SignUpView.as_view(), name='signup'),
    path('login_success/', login_success, name='login_success'),
    path('user/<int:pk>/agreement-list/', AgreementListView.as_view(), name='agreement_list'),
    path('user/<int:pk>/dogovor-ob-uchastii/', Agreement1.as_view(), name='agreement_1'),
    path('user/<int:pk>/dogovor-ssudy/', Agreement2.as_view(), name='agreement_2'),
    path('user/<int:pk>/prilojenie/', Agreement3.as_view(), name='agreement_3'),
    path('user/<int:pk>/prilojenie3/', AgreementPrilojenie3.as_view(),
         name='agreement_prilojenie_3'),
    path('user/<int:pk>/dogovor-o-zaloge/', Agreement4.as_view(), name='agreement_4'),
    path('user/<int:pk>/anketa/', Agreement5.as_view(), name='agreement_5'),
    path('user/<int:pk>/agreement-create/', AgreementCreateView.as_view(), name='agreement_create'),

    # Agreemt update
    path('user/<int:pk>/dogovor-ob-uchastii-update/', Agreement1UpdateView.as_view(), name='agreement1_update'),
    path('user/<int:pk>/dogovor-ssudy-update/', Agreement2UpdateView.as_view(), name='agreement2_update'),
    path('user/<int:pk>/prilojenie-update/', Agreement3UpdateView.as_view(), name='agreement3_update'),
    path('user/<int:pk>/dogovor-o-zaloge-update/', Agreement4UpdateView.as_view(), name='agreement4_update'),
    path('user/<int:pk>/anketa-update/', Agreement5UpdateView.as_view(), name='agreement5_update'),

    # Prilojenie
    path('user/<int:pk>/prilojenie-1/', Agreement_1.as_view(), name='agr_1'),
    path('user/<int:pk>/prilojenie-1-update/', Agreement_1UpdateView.as_view(), name='agr_1_update'),
    path('user/<int:pk>/prilojenie-2/', Agreement_2.as_view(), name='agr_2'),

    path('operator/create/', OperatorSignUp.as_view(), name='operator_signup'),
    path('operators/', OperatorsList.as_view(), name='operators_list'),
    path('operators/notification', NotificationList.as_view(), name='notification_list'),
    path('operator/notification/create/', NotificationCreate.as_view(), name='notification_create'),
    path('operator/notification/delete/<int:pk>', NotificationDelete.as_view(), name='notification_delete'),

    path('user/<int:pk>/reset_password/', user_password_reset, name='reset_user_password'),
]
