import json
import logging
import locale

from dateutil.relativedelta import relativedelta
from django.contrib import messages
from django.contrib.admin.models import LogEntry
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, DetailView, UpdateView, CreateView, TemplateView, DeleteView
from django.http import HttpResponse, HttpResponseRedirect
from dateutil.rrule import rrule, MONTHLY
from num2words import num2words
from ru_word2number import w2n

from quotes.models import Question
from users.forms import (CustomUserCreationForm, OperatorCreationForm,
                         CustomUserUpdateForm)
from users.models import CustomUser, UserAgreement, UserNotification

locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')
logger = logging.getLogger(__name__)


class SignUpView(UserPassesTestMixin, CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('users_list')
    template_name = 'registration/signup.html'

    def post(self, request, *args, **kwargs):
        LogEntry.objects.create(
            user=self.request.user,
            content_type=ContentType.objects.get_for_model(CustomUser),
            object_id=request.POST.get('username'),
            object_repr=request.POST.get('user_full_name'),
            action_flag=1,
            change_message=json.dumps({'Create new user': ['None', 'True']})
        )
        return super().post(request, *args, **kwargs)

    def test_func(self):
        logger.warning('User create with {}'.format(
            self.request.user.username))
        return self.request.user.is_superuser or self.request.user.user_is_operator


class OperatorSignUp(UserPassesTestMixin, CreateView):
    form_class = OperatorCreationForm
    model = CustomUser
    success_url = reverse_lazy('operators_list')
    template_name = 'registration/operator_signup.html'

    def test_func(self):
        logger.warning('Operator update with {}'.format(
            self.request.user.username))
        return self.request.user.is_superuser


class OperatorsList(UserPassesTestMixin, ListView):
    model = CustomUser
    template_name = 'users/operators_list.html'
    queryset = CustomUser.objects.order_by('-id').filter(
        Q(is_superuser=True) | Q(user_is_operator=True) | Q(is_moderator=True)).filter(~Q(username='manager'))
    paginate_by = 20

    def test_func(self):
        return self.request.user.is_superuser


class UserListView(UserPassesTestMixin, ListView):
    model = CustomUser
    template_name = 'users/users.html'
    queryset = CustomUser.objects.filter(is_superuser=False,
                                         user_is_operator=False,
                                         is_moderator=False,
                                         user_is_queued=True,
                                         user_is_delete=False
                                         ).order_by('-user_position')
    paginate_by = 50

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator

    def get_queryset(self):
        object_list = CustomUser.objects.filter(
            is_superuser=False,
            user_is_operator=False,
            is_moderator=False,
            user_is_queued=True,
            user_is_delete=False).order_by('-user_position')

        query = self.request.GET.get('q')
        if query is not None:
            object_list = object_list.filter(
                Q(username__icontains=query) |
                Q(user_full_name__icontains=query) |
                Q(user_inn__icontains=query) |
                Q(user_position__icontains=query) |
                Q(user_notifications__icontains=query)
            )

        return object_list

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        context['search_input'] = self.request.GET.get('q')
        return context


class UserGivenView(UserPassesTestMixin, ListView):
    model = CustomUser
    template_name = 'users/user_is_queued.html'
    queryset = CustomUser.objects.order_by('-id').filter(is_superuser=False, user_is_operator=False, is_moderator=False,
                                                         user_is_queued=False, user_is_delete=False).order_by('user_position')
    paginate_by = 50

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator

    def get_queryset(self):
        object_list = CustomUser.objects.order_by('-id').filter(is_superuser=False, user_is_operator=False, is_moderator=False,
                                                                user_is_queued=False, user_is_delete=False).order_by('user_position')
        query = self.request.GET.get('q')
        if query is not None:
            object_list = object_list.filter(
                Q(username__icontains=query) |
                Q(user_full_name__icontains=query) |
                Q(user_inn__icontains=query) |
                Q(user_position__icontains=query) |
                Q(user_notifications__icontains=query)
            )

        return object_list

    def get_context_data(self, **kwargs):
        context = super(UserGivenView, self).get_context_data(**kwargs)
        context['search_input'] = self.request.GET.get('q')
        return context


class UserDetail(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'users/user_detail.html'

    def test_func(self, **kwargs):
        return (self.request.user.is_superuser
                or self.request.user.pk == self.kwargs['pk']
                or self.request.user.user_is_operator)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        position = context['object'].user_position
        context['users'] = (
            CustomUser.objects.filter(
                user_is_operator=False,
                is_moderator=False,
                is_staff=False,
                user_is_queued=True)
            .order_by('user_position')[:position].iterator())
        question = (Question.objects.order_by(
            '-pub_date').values_list('pk', flat=True).first())
        question = (
            Question.objects.filter(
                pk=question, is_active=True
            ).values_list('pk', flat=True).first()
        )
        context['not_staff'] = not (context['object'].user_is_operator
                                    or context['object'].is_moderator)
        context['notification'] = UserNotification.objects.last()
        context['question'] = question
        return context


class UserDeleteView(UserPassesTestMixin, TemplateView):
    template_name = 'users/user_delete.html'
    success_url = reverse_lazy('users_given_list')

    def test_func(self):
        return self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = CustomUser.objects.get(pk=self.kwargs['pk'])
        return context


class UserUpdateView(UserPassesTestMixin, UpdateView):
    model = CustomUser
    form_class = CustomUserUpdateForm
    template_name = 'users/user_update.html'
    success_url = reverse_lazy('users_given_list')

    def post(self, request, *args, **kwargs):
        LogEntry.objects.create(
            user=self.request.user,
            content_type=ContentType.objects.get_for_model(CustomUser),
            object_id=request.POST.get('username'),
            object_repr=request.POST.get('user_full_name'),
            action_flag=2,
            change_message=json.dumps({'Update user': ['None', 'True']})
        )
        return super().post(request, *args, **kwargs)

    def test_func(self):
        logger.warning('User update with {}'.format(
            self.request.user.username))
        return self.request.user.is_superuser or self.request.user.user_is_operator


def login_success(request):
    if request.user.is_staff:
        return redirect('admin_page')
    elif request.user.user_is_operator:
        return redirect('users_list')
    elif request.user.is_moderator:
        return redirect('admin_page')
    else:
        return redirect('user_detail', pk=request.user.pk)


class AgreementListView(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'agreement/agreement_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_pk'] = CustomUser.objects.select_related(
            'user_agreement').get(pk=self.kwargs['pk'])
        return context

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class AgreementCreateView(UserPassesTestMixin, CreateView):
    model = UserAgreement
    template_name = 'agreement/agreement_create.html'
    fields = ('agreement_date', 'agreement_amount', 'agreement_month', 'agreement_home', 'agreement_purchase',
              'agreement_purchase_date', 'anketa_date_register', 'contract_date')
    success_url = reverse_lazy('users_list')

    def form_valid(self, form):
        user = CustomUser.objects.select_related(
            'user_agreement').get(pk=self.kwargs['pk'])
        form.instance.agreement_user = user
        return super(AgreementCreateView, self).form_valid(form)

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement1(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'agreement/agreement1.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            last_name = CustomUser.objects.get(
                pk=self.kwargs['pk']).user_full_name.split(' ')
            last = f'{last_name[0]} {last_name[1][0]}.{last_name[2][0]}.'
        except Exception as e:
            last = ''
            print('Except, ', e)

        context['25percent_text'] = w2n.word_to_num(
            self.object.user_agreement.home_price_25percent_text)
        context['4percent_text'] = w2n.word_to_num(
            self.object.user_agreement.home_price_4percent_text)
        context['last_name'] = last

        return context

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement2(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'agreement/agreement2.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        month = self.object.user_agreement.agreement_month
        year = self.object.user_agreement.contract_date

        summ = ''
        try:
            summ = self.object.user_agreement.agreement_amount.replace(" ", "")
        except Exception as e:
            print(e)

        # Пустые переменные
        start_date = ''
        assessed_pay = ''
        result = ''
        total_sum = []
        last_month = ''
        try:
            if year.day <= 15:
                start_date = year
            else:
                start_date = year + relativedelta(months=+ 1)
        except Exception as e:
            print(e, 'Empty assessed_pay')

        try:
            assessed_pay = start_date + relativedelta(months=int(month) + 1)
        except Exception as e:
            print(e, 'Empty assessed_pay')

        if summ and month:
            result = round((int(summ) / int(month)) + 0.2)

        try:
            if start_date and assessed_pay:
                for dt in rrule(MONTHLY, dtstart=start_date, until=assessed_pay):
                    summ = int(summ) - result
                    if summ > 0:
                        total_sum.append((summ - result) + result)
                        last_month = dt
        except Exception as e:
            print(e, 'year or accessed_pay is empty')

        last = ''
        try:
            last_name = self.object.user_full_name.split(' ')
            last = f'{last_name[0]} {last_name[1][0]}.{last_name[2][0]}.'
        except Exception as e:
            print('Except, ', e)

        context['last_month'] = last_month + relativedelta(months=1)
        context['last_name'] = last
        return context

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement3(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'agreement/agreement3.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        month = self.object.user_agreement.agreement_month
        year = self.object.user_agreement.contract_date

        summ = ''
        try:
            summ = self.object.user_agreement.agreement_amount.replace(" ", "")
        except Exception as e:
            print(e)

        # Пустые переменные
        start_date = ''
        assessed_pay = ''
        result = ''
        dates = []
        total_sum = []
        last_month = ''
        try:
            if year.day <= 15:
                start_date = year
            else:
                start_date = year + relativedelta(months=+ 1)
        except Exception as e:
            print(e, 'Empty assessed_pay')

        try:
            assessed_pay = start_date + relativedelta(months=int(month) + 1)
        except Exception as e:
            print(e, 'Empty assessed_pay')

        if summ and month:
            result = round((int(summ) / int(month)) + 0.2)

        try:
            if start_date and assessed_pay:
                for dt in rrule(MONTHLY, dtstart=start_date, until=assessed_pay):
                    dates.append(dt.strftime("%B %Y"))
                    summ = int(summ) - result
                    if summ > 0:
                        total_sum.append((summ - result) + result)
                        last_month = dt

        except Exception as e:
            print(e, 'year or accessed_pay is empty')

        if result:
            context['total'] = result
        if total_sum != result and total_sum[-1] > result:
            total_sum[-1] += (result - total_sum[-1])

        context['application_info'] = zip(dates, total_sum)
        context['last_month'] = last_month + relativedelta(months=1)
        context['last_summ'] = total_sum[-1]

        last = ''
        try:
            last_name = self.object.user_full_name.split(' ')
            last = f'{last_name[0]} {last_name[1][0]}.{last_name[2][0]}.'
        except Exception as e:
            print('Except, ', e)

        context['last_name'] = last
        context['assessed_pay'] = assessed_pay
        context['result'] = result
        return context

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class AgreementPrilojenie3(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'agreement/prilojenie.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        month = self.object.user_agreement.agreement_month
        year = self.object.user_agreement.contract_date

        summ = ''
        try:
            summ = self.object.user_agreement.agreement_amount.replace(" ", "")
        except Exception as e:
            print(e)

        # Пустые переменные
        start_date = ''
        assessed_pay = ''
        result = ''
        dates = []
        total_sum = []
        last_month = ''
        try:
            if year.day <= 15:
                start_date = year
            else:
                start_date = year + relativedelta(months=+ 1)
        except Exception as e:
            print(e, 'Empty assessed_pay')

        try:
            assessed_pay = start_date + relativedelta(months=int(month) + 1)
        except Exception as e:
            print(e, 'Empty assessed_pay')

        if summ and month:
            result = round((int(summ) / int(month)) + 0.2)

        try:
            if start_date and assessed_pay:
                for dt in rrule(MONTHLY, dtstart=start_date,
                                until=assessed_pay):
                    dates.append(dt.strftime("%B %Y"))
                    summ = int(summ) - result
                    if summ > 0:
                        total_sum.append((summ - result) + result)
                        last_month = dt

        except Exception as e:
            print(e, 'year or accessed_pay is empty')

        if result:
            context['total'] = result
        if total_sum != result:
            total_sum[-1] += (result - total_sum[-1])

        context['application_info'] = zip(dates, total_sum)
        context['last_month'] = last_month + relativedelta(months=1)
        context['last_summ'] = total_sum[-1]

        last = ''
        try:
            last_name = self.object.user_full_name.split(' ')
            last = f'{last_name[0]} {last_name[1][0]}.{last_name[2][0]}.'
        except Exception as e:
            print('Except, ', e)

        context['last_name'] = last
        context['assessed_pay'] = assessed_pay
        context['result'] = result
        return context

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement4(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'agreement/agreement4.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        last = ''
        try:
            last_name = self.object.user_full_name.split(' ')
            last = f'{last_name[0]} {last_name[1][0]}.{last_name[2][0]}.'
        except Exception as e:
            print('Except, ', e)

        context['last_name'] = last
        return context

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement5(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'agreement/agreement5.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        last = ''
        try:
            last_name = self.object.user_full_name.split(' ')
            last = f'{last_name[0]} {last_name[1][0]}.{last_name[2][0]}.'
        except Exception as e:
            print('Except, ', e)

        context['last_name'] = last
        return context

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement_1(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'agreement/dogovor_1.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.object.user_agreement.prilojenie_date
        months = self.object.user_agreement.prilojenie_count
        price = self.object.user_agreement.home_month_price
        summ = 0

        # Пустые переменные
        start_date = year
        assessed_pay = ''
        dates = []
        total_sum = []

        try:
            assessed_pay = start_date + relativedelta(months=months - 1)
        except Exception as e:
            print(e, 'Empty assessed_pay')

        try:
            if start_date and assessed_pay:
                for dt in rrule(MONTHLY, dtstart=start_date, until=assessed_pay):
                    dates.append(dt.strftime("%d %B %Y"))
                    summ = int(summ) + price
                    if summ > 0:
                        total_sum.append(summ)
                        last_month = dt
        except Exception as e:
            print(e, 'year or accessed_pay is empty')

        last = ''
        try:
            last_name = self.object.user_full_name.split(' ')
            last = f'{last_name[0]} {last_name[1][0]}.{last_name[2][0]}.'
        except Exception as e:
            print('Except, ', e)

        context['application_info'] = zip(dates, total_sum)
        context['last_name'] = last
        context['last_summ'] = summ
        return context

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement_2(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'agreement/dogovor_2.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.object.user_agreement.prilojenie_date
        months = self.object.user_agreement.prilojenie_count
        price = self.object.user_agreement.home_month_price
        summ = 0

        # Пустые переменные
        start_date = year
        assessed_pay = ''
        dates = []
        total_sum = []

        try:
            assessed_pay = start_date + relativedelta(months=11)

        except Exception as e:
            print(e, 'Empty assessed_pay')

        try:
            if start_date and assessed_pay:
                for dt in rrule(MONTHLY, dtstart=start_date, until=assessed_pay):
                    dates.append(dt.strftime("%B %Y"))
                    summ = int(summ) + price
                    if summ > 0:
                        total_sum.append(summ)
                        last_month = dt
        except Exception as e:
            print(e, 'year or accessed_pay is empty')

        last = ''
        try:
            last_name = self.object.user_full_name.split(' ')
            last = f'{last_name[0]} {last_name[1][0]}.{last_name[2][0]}.'
        except Exception as e:
            print('Except, ', e)

        context['application_info'] = zip(dates[6:12], total_sum[6:12])
        context['last_name'] = last
        context['last_summ'] = summ
        return context

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement1UpdateView(UserPassesTestMixin, UpdateView):
    model = UserAgreement
    template_name = 'agreement/agreement1_update.html'
    fields = ['agreement_date', 'agreement_city', 'agreement_document_number', 'agreement_chairman',
              'home_price', 'home_price_text', "home_price_25percent_text", 'home_price_4percent_text',
              'loan_amount_text', "home_month_price",
              "law_payment", "law_payment_text", "home_month_price",
              "percent_25_field", "percent_4_field", "percent_25_field_text",
              "percent_4_field_text",
              ]
    success_url = reverse_lazy('users_list')

    def get_success_url(self):
        user_pk = CustomUser.objects.get(user_agreement=self.object.pk).pk
        return reverse_lazy('agreement_list', kwargs={'pk': user_pk})

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement_1UpdateView(UserPassesTestMixin, UpdateView):
    model = UserAgreement
    template_name = 'agreement/agreement_1_update.html'
    fields = ['prilojenie_date', 'prilojenie_count', 'prilojenie_count_text']
    success_url = reverse_lazy('users_list')

    def get_success_url(self):
        user_pk = CustomUser.objects.get(user_agreement=self.object.pk).pk
        return reverse_lazy('agreement_list', kwargs={'pk': user_pk})

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement2UpdateView(UserPassesTestMixin, UpdateView):
    model = UserAgreement
    template_name = 'agreement/agreement2_update.html'
    fields = ['contract_date', 'contract_city', 'agreement_amount',
              'agreement_month', 'agreement_home', 'agreement_amount_text',
              'agreement_month_text', 'agreement_chairman2']

    def get_success_url(self):
        user_pk = CustomUser.objects.get(user_agreement=self.object.pk).pk
        return reverse_lazy('agreement_list', kwargs={'pk': user_pk})

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement3UpdateView(UserPassesTestMixin, UpdateView):
    model = UserAgreement
    template_name = 'agreement/agreement3_update.html'
    fields = ['application_date', 'agreement_amount', 'agreement_month']

    def get_success_url(self):
        user_pk = CustomUser.objects.get(user_agreement=self.object.pk).pk
        return reverse_lazy('agreement_list', kwargs={'pk': user_pk})

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement4UpdateView(UserPassesTestMixin, UpdateView):
    model = UserAgreement
    template_name = 'agreement/agreement4_update.html'
    fields = ['agreement_city', 'deposit_date', 'deposit_date_text', 'agreement_amount', 'agreement_amount_text', 'agreement_home',
              'law_district', 'agreement_by_whom', 'agreement_month', 'agreement_month_text', 'deposite_by']

    def get_success_url(self):
        user_pk = CustomUser.objects.get(user_agreement=self.object.pk).pk
        return reverse_lazy('agreement_list', kwargs={'pk': user_pk})

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class Agreement5UpdateView(UserPassesTestMixin, UpdateView):
    model = UserAgreement
    template_name = 'agreement/agreement5_update.html'
    fields = ['anketa_date_register']

    def get_success_url(self):
        user_pk = CustomUser.objects.get(user_agreement=self.object.pk).pk
        return reverse_lazy('agreement_list', kwargs={'pk': user_pk})

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


def user_to_delete_list(request, pk):
    if request.method == 'POST':
        user = CustomUser.objects.get(pk=pk)
        user.user_is_delete = True
        user.save()
        return HttpResponseRedirect(reverse('users_given_list'))
    else:
        return HttpResponse('GET', status=405)


def user_password_reset(request, pk):
    user = get_object_or_404(CustomUser, pk=pk)
    user.set_password('Kelechek123')
    user.save()
    messages.success(request, f"Пароль успешно изменен")
    return HttpResponseRedirect(reverse('user_detail', kwargs={'pk': pk}))


class NotificationList(UserPassesTestMixin, ListView):
    model = UserNotification
    template_name = 'notification/notification_list.html'
    queryset = UserNotification.objects.all().order_by('-create_at')
    paginate_by = 20

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_operator


class NotificationCreate(UserPassesTestMixin, CreateView):
    model = UserNotification
    success_url = reverse_lazy('notification_list')
    fields = ('title', 'vote_file', 'description')
    template_name = 'notification/notification_create.html'

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_operator


class NotificationDelete(UserPassesTestMixin, DeleteView):
    model = UserNotification
    template_name = 'notification/notification_delete.html'
    success_url = reverse_lazy('notification_list')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_operator