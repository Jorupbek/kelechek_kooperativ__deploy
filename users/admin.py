from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.admin.models import LogEntry
from django.db.models import Q
from import_export.admin import ImportExportModelAdmin
from users.models import UserAgreement, CustomUser, UserNotification


class BusinessEmailFilter(SimpleListFilter):
    """
    This filter is being used in django admin panel in profile model.
    """
    title = 'Фильтр пользователей'
    parameter_name = 'user__is_admin'

    def lookups(self, request, model_admin):
        return (
            ('approoved', 'Список Участников'),
            ('non_approoved', 'Список получивших'),
            ('moderators', "Список администрации"),
        )

    def queryset(self, request, queryset):
        if not self.value():
            return queryset
        if self.value().lower() == 'approoved':
            return queryset.filter(is_superuser=False,
                                   user_is_operator=False,
                                   is_moderator=False,
                                   user_is_queued=True,
                                   user_is_delete=False
                                   ).order_by('-id')
        elif self.value().lower() == 'non_approoved':
            return queryset.filter(is_superuser=False,
                                   user_is_operator=False,
                                   is_moderator=False,
                                   user_is_queued=False,
                                   user_is_delete=False
                                   ).order_by('-id')
        elif self.value().lower() == 'moderators':
            return queryset.filter(Q(user_is_operator=True) |
                                   Q(is_moderator=True)
                                   ).order_by('-id')


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ('user_full_name', 'user_phone_number1', 'user_position',
                    'id')
    list_display_links = ('user_full_name', 'user_phone_number1', 'id')
    list_editable = ['user_position']
    search_fields = ('user_full_name', 'username', 'user_inn')
    readonly_fields = ('update_at',)
    list_filter = (BusinessEmailFilter,)


class UserAgreementAdmin(ImportExportModelAdmin):
    list_display = (
        'id', 'agreement_city', 'agreement_amount', 'agreement_home')
    list_display_links = (
        'id', 'agreement_city', 'agreement_amount', 'agreement_home')


@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    date_hierarchy = 'action_time'

    list_filter = [
        'action_flag'
    ]

    search_fields = [
        'object_repr',
        'change_message'
    ]

    list_display = [
        'action_time',
        'user',
        'content_type',
        'action_flag',
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return request.user.is_superuser


admin.site.register(UserAgreement, UserAgreementAdmin)
admin.site.register(UserNotification)
