# Generated by Django 3.0 on 2021-04-22 21:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20210423_0338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='user_region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='region_for_user', to='users.CustomUserRegion'),
        ),
    ]
