# Generated by Django 3.0 on 2021-04-22 22:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_auto_20210423_0443'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='user_region',
            field=models.ForeignKey(blank=True, default=1, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='region_for_user', to='users.CustomUserRegion'),
        ),
        migrations.AlterField(
            model_name='customuserregion',
            name='region_name',
            field=models.CharField(default=1, max_length=255, unique=True, verbose_name='Регион'),
            preserve_default=False,
        ),
    ]
