import time

from django.contrib.auth.models import AbstractUser
from django.db import models, transaction
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from region.models import UserRegion

BOOKKEEPER_CHOICE = [
    ('Первоначальный взнос', 'Первоначальный взнос'),
    ('Пополнение', 'Пополнение'),
    ('Вступительный взнос', 'Вступительный взнос'),
    ('Погашение ссуды', 'Погашение ссуды'),
]


class CustomUser(AbstractUser):
    username = models.CharField(verbose_name='Номер паспорта',
                                max_length=255, unique=True)
    user_full_name = models.CharField(max_length=150,
                                      verbose_name='ФИО',
                                      blank=True, null=True)
    user_full_name_plural = models.CharField(max_length=150,
                                             verbose_name='ФИО в Родительском падеже',
                                             blank=True, null=True)
    user_passport_given_by = models.CharField(max_length=30,
                                              verbose_name='Выдан кем')
    user_passport_from_date = models.DateField(verbose_name='Выдан от',
                                               blank=True, null=True)
    user_inn = models.CharField(max_length=20, verbose_name='ИНН')
    user_birthday = models.DateField(verbose_name='День рождения',
                                     blank=True, null=True)
    user_birthday_city = models.CharField(max_length=200,
                                          verbose_name='Место рождения')
    user_passport_address = models.CharField(max_length=200,
                                             verbose_name='Адрес по прописке')
    user_real_address = models.CharField(max_length=200,
                                         verbose_name='Фактический адрес проживания')
    user_phone_number1 = models.CharField(max_length=255,
                                          verbose_name='Номер телефона 1')
    user_phone_number2 = models.CharField(max_length=255,
                                          verbose_name='Номер телефона 2',
                                          null=True, blank=True)
    user_registration_date = models.DateTimeField('Дата регистрации',
                                                  auto_now_add=True,
                                                  blank=True,
                                                  null=True)
    # status if user is an operator
    user_is_operator = models.BooleanField(default=False,
                                           verbose_name='Статус оператора')
    is_moderator = models.BooleanField('Статус модератора', default=False)
    is_bookkeeper = models.BooleanField('Статус бухгалтера', default=False)
    # status if user has already got the property
    user_is_queued = models.BooleanField(default=True,
                                         verbose_name='Пользователь в Очереди')
    user_balance = models.CharField(max_length=50, verbose_name='Баланс',
                                    blank=True, null=True)
    user_position = models.IntegerField(verbose_name='Позиция',
                                        blank=True, null=True, default=1)
    user_notifications = models.TextField(verbose_name='Оповещения',
                                          blank=True, null=True)
    user_agreement = models.ForeignKey('UserAgreement',
                                       on_delete=models.CASCADE,
                                       null=True, blank=True,
                                       related_name='agreement_user_model')
    user_region = models.ForeignKey(UserRegion,
                                    on_delete=models.CASCADE,
                                    related_name='region_for_user',
                                    null=True, blank=True)
    user_is_delete = models.BooleanField(default=False,
                                         verbose_name='Пользователь удален')
    update_at = models.DateTimeField('Дата послендего изменения',
                                     auto_now=True, null=True, blank=True)

    def __str__(self):
        result = ''
        last_name = (
            self.user_full_name.split()
            if self.user_full_name
            else self.user_full_name)
        try:
            if last_name:
                if len(last_name) == 3:
                    result = f'{last_name[0]} {last_name[1][0]}. {last_name[2][0]}.'
                elif len(last_name) == 2:
                    result = f'{last_name[0]} {last_name[1][0]}.'
        except:
            result = f'{last_name[0]}' if last_name[0] else last_name
        return result

    def clean(self):
        self.username = self.username.lower()


class UserAgreement(models.Model):
    agreement_city = models.CharField('Город договора', max_length=255,
                                      help_text='пример: г. Бишкек',
                                      null=True, blank=True)
    agreement_by_whom = models.TextField('В лице от кооператива',
                                         null=True, blank=True)
    agreement_date = models.DateField('Дата Договора об участии',
                                      null=True, blank=True)
    agreement_document_number = models.IntegerField('№ Договора об участии',
                                                    null=True, blank=True)
    agreement_amount = models.CharField('ссуда на сумму', max_length=255,
                                        help_text="787 500",
                                        null=True, blank=True)
    agreement_amount_text = models.CharField('ссуда на сумму в виде текста',
                                             null=True, blank=True,
                                             help_text="Семьсот восемьдесят семь тысяч пятьсот",
                                             max_length=255)
    agreement_month = models.IntegerField('Месяцев на ссуду',
                                          help_text='120', default=0,
                                          null=True, blank=True)
    agreement_month_text = models.CharField('Месяцев на ссуду в виде текста',
                                            help_text='сто двадцать',
                                            max_length=255,
                                            null=True, blank=True)
    agreement_home = models.TextField('Залоговое имущество',
                                      null=True, blank=True,
                                      help_text='''Квартира, полезной площадью 34,1 кв.м., 
                                    жилой площадью 18,2 кв.м., находящийся по адресу''')
    agreement_purchase = models.CharField('Договор купли-продажи №',
                                          max_length=255,
                                          null=True, blank=True)
    agreement_purchase_date = models.DateField('Дата Договора купли-продажи',
                                               null=True, blank=True)
    agreement_chairman = models.CharField(verbose_name='В лице Председателя',
                                          max_length=255, null=True,
                                          blank=True,
                                          help_text='в лице Председателя Правления Курманалиева Эламана Курманалиевича')
    agreement_chairman2 = models.CharField(verbose_name='В лице Председателя',
                                           max_length=255, null=True,
                                           blank=True,
                                           help_text='''в лице Председателя Правления 
                                    Курманалиева Эламана Курманалиевича''')
    home_price = models.IntegerField(default=0,
                                     verbose_name='Желаемая стоимость',
                                     help_text='Желаемая стоимость недвижимого имущества')
    home_month_price = models.IntegerField(default=0,
                                           help_text='Член кооператива оплачивает сумму в размере',
                                           verbose_name='Оплачиваемся сумма')
    home_price_text = models.CharField(max_length=255,
                                       verbose_name='Желаемая стоимость в письменном виде',
                                       help_text='''Желаемая стоимость недвижимого 
                                    имущества в письменном виде (два миллиона сомов)''')
    home_price_25percent_text = models.CharField(max_length=255,
                                                 verbose_name='25% от общей суммы в письменном виде',
                                                 help_text='25% от общей суммы в письменном виде')
    home_price_4percent_text = models.CharField(max_length=255,
                                                verbose_name='4% от общей суммы в письменном виде',
                                                help_text='4% от общей суммы в письменном виде')
    loan_amount_text = models.CharField('Оплачиваемая сумма в письменном виде',
                                        max_length=255, null=True, blank=True)
    prilojenie_date = models.DateField('Дата приложения',
                                       null=True, blank=True)
    prilojenie_count = models.IntegerField('Количество месяцев', default=0,
                                           null=True, blank=True)
    prilojenie_count_text = models.CharField('Количество месяцев текст',
                                             max_length=255, null=True,
                                             blank=True,
                                             help_text='''Текст количество 
                                             месяцев (6 месяцев)''')
    anketa_date_register = models.DateField('Дата регистрации анкеты',
                                            null=True, blank=True)
    contract_date = models.DateField('Дата договора ссуды',
                                     null=True, blank=True)
    contract_city = models.CharField('Город договора ссуды',
                                     help_text='г. Бишкек',
                                     max_length=255, null=True, blank=True)
    deposit_date = models.DateField('Дата договора о залоге',
                                    null=True, blank=True)
    deposit_date_text = models.CharField(
        'Дата договора о залоге в письменном виде',
        max_length=255, null=True, blank=True,
        help_text="""
                                         Двадцатое сентрября две тысячри двадцать второго года""")
    application_date = models.DateField('Дата приложения',
                                        null=True, blank=True)
    law_district = models.CharField('Регион нотариуса',
                                    max_length=255, null=True, blank=True)
    bookkeeper_choice = models.CharField(max_length=255,
                                         choices=BOOKKEEPER_CHOICE, null=True,
                                         blank=True,
                                         default='Первоначальный взнос',
                                         verbose_name="Выбор взноса")
    deposite_by = models.CharField(max_length=255, null=True, blank=True,
                                   verbose_name="По дов")
    law_payment = models.IntegerField('юридическое сопровождение', default=0,
                                      null=True, blank=True)
    law_payment_text = models.CharField('юридическое сопровождение текст',
                                        max_length=255, null=True, blank=True)
    percent_25_field = models.IntegerField("25%", null=True, blank=True,
                                           default=25)
    percent_25_field_text = models.CharField(
        '25% в виде текст (двадцвать пять процентов)',
        max_length=255, null=True, blank=True)
    percent_4_field = models.IntegerField("4%", null=True, blank=True,
                                          default=4)
    percent_4_field_text = models.CharField(
        '4%  в виде текст (четыре процента)',
        max_length=255, null=True, blank=True)

    def __str__(self):
        return f'{self.id}'

    def percent_25(self):
        percent = self.percent_25_field / 100
        return int(self.home_price * percent)

    def percent_4(self):
        percent = self.percent_4_field / 100
        return int(self.home_price * percent)


@receiver(pre_save, sender=CustomUser)
def user_check(sender, instance, **kwargs):
    currentUser = sender.objects.filter(id=instance.id).first()
    users = CustomUser.objects.only('user_position', 'id').filter(
        is_staff=False, user_is_operator=False, is_superuser=False,
        user_is_queued=True, is_moderator=False
    )
    if (currentUser and not instance.user_is_operator
            and not instance.is_moderator
            and not instance.is_staff):
        if currentUser.user_is_queued != instance.user_is_queued:
            with transaction.atomic():
                users_list = []
                for x in users:
                    if instance.user_is_queued:
                        if x.user_position >= instance.user_position:
                            x.user_position += 1
                            users_list.append(x)
                    else:
                        if x.user_position > instance.user_position:
                            x.user_position -= 1
                            users_list.append(x)
                CustomUser.objects.bulk_update(users_list, ['user_position'])
    elif (currentUser is None
          and not instance.user_is_operator
          and not instance.is_moderator
          and not instance.is_staff):
        with transaction.atomic():
            new_obj = CustomUser.objects.filter(user_is_queued=True,
                                                is_staff=False,
                                                user_is_operator=False,
                                                is_moderator=False,
                                                is_superuser=False).values_list(
                'user_position', flat=True).order_by('user_position').last()
            instance.user_position = new_obj + 1
            return
    else:
        with transaction.atomic():
            for x in users:
                if not instance.user_is_operator and not instance.is_staff \
                        and not instance.is_moderator:
                    x.user_position += 1
                    x.save()


@receiver(post_save, sender=CustomUser)
def user_post_save(sender, instance, created, **kwargs):
    if created:
        agr = UserAgreement.objects.create()
        user = CustomUser.objects.get(id=instance.id)
        user.user_agreement = agr
        user.save()


class UserNotification(models.Model):
    title = models.CharField(
        verbose_name='Заголовок для оповещения', max_length=400)
    vote_file = models.FileField('Документ для оповещения', null=True, blank=True, 
                                     upload_to='vote')
    description = models.TextField(verbose_name='Описание')
    create_at = models.DateTimeField('Дата оповещения',
                                     auto_now_add=True)

    def __str__(self) -> str:
        return self.title
