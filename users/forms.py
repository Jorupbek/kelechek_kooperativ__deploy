from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.core.exceptions import ValidationError
from django import forms

from region.models import UserRegion
from users.models import CustomUser


class OperatorCreationForm(UserCreationForm):
    username = forms.CharField(label='Логин', required=True, error_messages={'required': "Это поле пустое"},
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control'}
    ))

    user_phone_number1 = forms.CharField(label='Номер телефона 1', required=True,
                                         error_messages={
                                             'required': "Это поле пустое"},
                                         widget=forms.TextInput(
                                             attrs={'class': 'form-control'}
                                         ))
    password1 = forms.CharField(label='Пароль', error_messages={'required': "Это поле пустое"}, required=True,
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control', 'placeholder': 'Пароль'}), )
    password2 = forms.CharField(label='Повторите пароль', error_messages={'required': "Это поле пустое"}, required=True,
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control', 'placeholder': 'Повторите пароль'}))

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields + (
            'username', 'user_phone_number1', 'user_is_operator', 'is_moderator')

    def clean_username(self):
        username = self.cleaned_data['username']
        r = CustomUser.objects.filter(username=username)
        if r.count():
            raise ValidationError("Такой логин уже используется")
        return username

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Пароли не совпадают")

        return password2


class CustomUserCreationForm(UserCreationForm):
    username = forms.CharField(label='Номер паспорта', required=True,
                               error_messages={'required': "Это поле пустое"},
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control'}
                               ))
    user_full_name = forms.CharField(label='ФИО', required=True, error_messages={'required': "Это поле пустое"},
                                     widget=forms.TextInput(
                                         attrs={'class': 'form-control'}
    ))
    user_full_name_plural = forms.CharField(label='ФИО в Родительском падеже', required=True,
                                            error_messages={
                                                'required': "Это поле пустое"},
                                            widget=forms.TextInput(
                                                attrs={'class': 'form-control'}
                                            ))
    user_passport_given_by = forms.CharField(label='Выдан кем', required=True,
                                             error_messages={
                                                 'required': "Это поле пустое"},
                                             widget=forms.TextInput(
                                                 attrs={
                                                     'class': 'form-control'}
                                             ))
    user_passport_from_date = forms.DateField(label='Выдан от', required=True,
                                              error_messages={
                                                  'required': "Это поле пустое"},
                                              widget=forms.DateInput(
                                                  attrs={
                                                      'class': 'form-control', 'type': 'date'}
                                              ))
    user_inn = forms.CharField(label='ИНН', required=True, error_messages={'required': "Это поле пустое"},
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control'}
    ))
    user_birthday = forms.DateField(label='День рождения', required=True,
                                    error_messages={
                                        'required': "Это поле пустое"},
                                    widget=forms.DateInput(
                                        attrs={'class': 'form-control',
                                               'type': 'date'}
                                    ))
    user_birthday_city = forms.CharField(label='Место рождения', required=True,
                                         error_messages={
                                             'required': "Это поле пустое"},
                                         widget=forms.TextInput(
                                             attrs={'class': 'form-control'}
                                         ))
    user_passport_address = forms.CharField(label='Адрес по прописке', required=True,
                                            error_messages={
                                                'required': "Это поле пустое"},
                                            widget=forms.TextInput(
                                                attrs={'class': 'form-control'}
                                            ))
    user_real_address = forms.CharField(label='Фактический адрес проживания', required=True,
                                        error_messages={
                                            'required': "Это поле пустое"},
                                        widget=forms.TextInput(
                                            attrs={'class': 'form-control'}
                                        ))
    user_phone_number1 = forms.CharField(label='Номер телефона 1', required=True,
                                         error_messages={
                                             'required': "Это поле пустое"},
                                         widget=forms.TextInput(
                                             attrs={'class': 'form-control'}
                                         ))
    user_region = forms.ModelChoiceField(label='Регион пользователя', required=True, initial=1,
                                        queryset=UserRegion.objects.all())
    user_balance = forms.CharField(label='Баланс', required=True, error_messages={'required': "Это поле пустое"},
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control'}
    ))
    user_notifications = forms.CharField(label='Оповещения', required=True,
                                         error_messages={
                                             'required': "Это поле пустое"},
                                         widget=forms.Textarea(
                                             attrs={'class': 'form-control'}
                                         ))
    password1 = forms.CharField(label='Пароль', error_messages={'required': "Это поле пустое"}, required=True,
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control', 'placeholder': 'Пароль'}), )
    password2 = forms.CharField(label='Повторите пароль', error_messages={'required': "Это поле пустое"}, required=True,
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control', 'placeholder': 'Повторите пароль'}))

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields + (
            'username', 'user_full_name', 'user_full_name_plural', 'user_passport_given_by',
            'user_passport_from_date', 'user_inn', 'user_birthday', 'user_birthday_city',
            'user_passport_address', 'user_real_address', 'user_phone_number1', 'user_phone_number2', 'user_balance',
            'user_notifications', 'user_region')

    def clean_username(self):
        username = self.cleaned_data['username']
        r = CustomUser.objects.filter(username=username)
        if r.count():
            raise ValidationError("Такой логин уже используется")
        return username

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Пароли не совпадают")

        return password2


class CustomUserUpdateForm(UserChangeForm):
    username = forms.CharField(label='Номер паспорта', required=True,
                               error_messages={'required': "Это поле пустое"},
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control'}
                               ))
    user_full_name = forms.CharField(label='ФИО', required=True, error_messages={'required': "Это поле пустое"},
                                     widget=forms.TextInput(
                                         attrs={'class': 'form-control'}
    ))
    user_full_name_plural = forms.CharField(label='ФИО в Родительском падеже', required=True,
                                            error_messages={
                                                'required': "Это поле пустое"},
                                            widget=forms.TextInput(
                                                attrs={'class': 'form-control'}
                                            ))
    user_passport_given_by = forms.CharField(label='Выдан кем', required=True,
                                             error_messages={
                                                 'required': "Это поле пустое"},
                                             widget=forms.TextInput(
                                                 attrs={
                                                     'class': 'form-control'}
                                             ))

    user_inn = forms.CharField(label='ИНН', required=True, error_messages={'required': "Это поле пустое"},
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control'}
    ))

    user_birthday_city = forms.CharField(label='Место рождения', required=True,
                                         error_messages={
                                             'required': "Это поле пустое"},
                                         widget=forms.TextInput(
                                             attrs={'class': 'form-control'}
                                         ))
    user_passport_address = forms.CharField(label='Адрес по прописке', required=True,
                                            error_messages={
                                                'required': "Это поле пустое"},
                                            widget=forms.TextInput(
                                                attrs={'class': 'form-control'}
                                            ))
    user_real_address = forms.CharField(label='Фактический адрес проживания', required=True,
                                        error_messages={
                                            'required': "Это поле пустое"},
                                        widget=forms.TextInput(
                                            attrs={'class': 'form-control'}
                                        ))
    user_phone_number1 = forms.CharField(label='Номер телефона 1', required=True,
                                         error_messages={
                                             'required': "Это поле пустое"},
                                         widget=forms.TextInput(
                                             attrs={'class': 'form-control'}
                                         ))
    user_phone_number2 = forms.CharField(label='Номер телефона 2', required=False,
                                         widget=forms.TextInput(
                                             attrs={'class': 'form-control'}
                                         ))
    user_balance = forms.CharField(label='Баланс', required=True, error_messages={'required': "Это поле пустое"},
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control'}
    ))
    user_notifications = forms.CharField(label='Оповещения', required=True,
                                         error_messages={
                                             'required': "Это поле пустое"},
                                         widget=forms.Textarea(
                                             attrs={'class': 'form-control'}
                                         ))
    user_region = forms.ModelChoiceField(label='Регион пользователя', required=True, initial=1,
                                        queryset=UserRegion.objects.all())

    class Meta:
        model = CustomUser
        fields = ['username', 'user_full_name', 'user_full_name_plural',
                  'user_passport_given_by', 'user_passport_from_date', 'user_inn',
                  'user_is_queued', 'user_birthday', 'user_birthday_city', 'user_passport_address', 'user_real_address',
                  'user_phone_number1', 'user_phone_number2', 'user_balance', 'user_notifications',
                  'is_moderator', 'user_is_operator', 'user_region']
