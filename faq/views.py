from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, DeleteView, UpdateView

from faq.forms import FaqForm
from faq.models import Faq


class FaqCreateView(CreateView):
    model = Faq
    form_class = FaqForm
    template_name = 'faq/faq.html'
    success_url = reverse_lazy('home')


class FaqReceivedListView(UserPassesTestMixin, ListView):
    model = Faq
    template_name = 'faq/faq_received_list.html'
    queryset = Faq.objects.order_by('-id').filter(faq_status=False)
    context_object_name = 'received_faqs'
    paginate_by = 20

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator


class FaqPublishedListView(UserPassesTestMixin, ListView):
    model = Faq
    template_name = 'faq/faq_published_list.html'
    queryset = Faq.objects.filter(faq_status=True).order_by('-id')
    context_object_name = 'published_faqs'

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator


class FaqDetail(DetailView):
    model = Faq
    template_name = 'faq/faq_detail.html'


class FaqDeleteView(UserPassesTestMixin, DeleteView):
    model = Faq
    template_name = 'faq/faq_delete.html'
    success_url = reverse_lazy('faq_received')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator


class FaqUpdateView(UserPassesTestMixin, UpdateView):
    model = Faq
    template_name = 'faq/faq_update.html'
    fields = ['faq_name', 'faq_phone', 'faq_question',
              'faq_answer', 'faq_status']
    success_url = reverse_lazy('faq_received')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator
