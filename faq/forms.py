from captcha.fields import CaptchaField
from django import forms
from .models import Faq


class FaqForm(forms.ModelForm):
    faq_name = forms.CharField(label='ФИО', required=True,
                               widget=forms.TextInput(
                                    attrs={'class': 'form-control'}))
    faq_phone = forms.CharField(label='Телефон', required=True,
                                widget=forms.TextInput(
                                   attrs={'class': 'form-control'}))
    faq_question = forms.CharField(label='Вопрос', required=True,
                                   widget=forms.TextInput(
                                    attrs={'class': 'form-control'}))
    captcha = CaptchaField()

    class Meta:
        model = Faq
        fields = ['faq_name', 'faq_phone', 'faq_question', 'captcha']
