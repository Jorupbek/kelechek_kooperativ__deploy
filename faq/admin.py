from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from faq.models import Faq


class FaqAdmin(ImportExportModelAdmin):
    list_display = ('faq_name', 'faq_phone', 'faq_status', )
    list_display_links = ('faq_name', 'faq_phone', 'faq_status', )
    list_filter = ('faq_name', 'faq_phone', 'faq_date')
    date_hierarchy = 'faq_date'
    save_on_top = True
    actions_on_top = True


admin.site.register(Faq, FaqAdmin)
