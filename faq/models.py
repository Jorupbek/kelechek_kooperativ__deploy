from django.db import models
from _datetime import datetime


class Faq(models.Model):
    faq_name = models.CharField(max_length=150, verbose_name="ФИО")
    faq_phone = models.CharField(max_length=50, verbose_name="Телефон")
    faq_question = models.TextField(verbose_name="Вопрос")
    faq_answer = models.TextField(blank=True, null=True, verbose_name="Ответ")
    faq_status = models.BooleanField(default=False, verbose_name="Опубликовать")
    faq_date = models.DateTimeField(default=datetime.now, verbose_name="Дата")

    def __str__(self):
        return self.faq_name
