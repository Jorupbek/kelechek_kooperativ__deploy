from .models import Faq


def faq_context(request):
    published_faq = Faq.objects.filter(faq_status=True).order_by('-id')
    return {'faq_context': published_faq}
