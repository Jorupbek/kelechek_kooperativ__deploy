from django.urls import path
from .views import (FaqCreateView, FaqReceivedListView, FaqDetail,
                    FaqDeleteView, FaqUpdateView, FaqPublishedListView)

urlpatterns = [
    path('faq/', FaqCreateView.as_view(), name='faq'),
    path('faq_received/', FaqReceivedListView.as_view(), name='faq_received'),
    path('faq_published/', FaqPublishedListView.as_view(), name='faq_published'),
    path('faq/<int:pk>/', FaqDetail.as_view(), name='faq_detail'),
    path('faq/<int:pk>/delete', FaqDeleteView.as_view(), name='faq_delete'),
    path('faq/<int:pk>/update', FaqUpdateView.as_view(), name='faq_update'),
]
