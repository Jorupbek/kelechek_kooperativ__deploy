from rest_framework import viewsets, permissions
from rest_framework.pagination import PageNumberPagination

from api.serializers import *

from users.models import CustomUser


class NewsList(viewsets.ModelViewSet):
    serializer_class = NewsSerializer
    queryset = News.objects.all().order_by('-news_date')


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 150


class UserList(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserSerializer

    def get_queryset(self, *args, **kwargs):
        query_user_id = self.request.GET.get("user_id")
        query_user_position = self.request.GET.get("user_position")
        if query_user_id:
            queryset_list = CustomUser.objects.select_related('user_region').filter(
                id__exact=query_user_id
            )
        elif query_user_position:
            queryset_list = CustomUser.objects.select_related('user_region').filter(
                user_position__lte=query_user_position,
                user_is_operator=False,
                is_moderator=False,
                is_bookkeeper=False,
                user_is_queued=True,
                user_is_delete=False
            ).order_by('-user_position')
        else:
            queryset_list = CustomUser.objects.filter(
                user_is_operator=False,
                is_moderator=False,
                is_bookkeeper=False,
                user_is_queued=True,
                user_is_delete=False
            ).order_by('-user_position')
        return queryset_list


class FAQList(viewsets.ModelViewSet):
    serializer_class = FAQSerializer
    queryset = Faq.objects.filter(faq_status=True)
