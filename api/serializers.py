from rest_framework import serializers
from dj_rest_auth.serializers import TokenSerializer

from faq.models import Faq
from news.models import News
from users.models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    region_name = serializers.CharField(source='user_region.region_name')

    class Meta:
        model = CustomUser
        fields = ('id', 'user_full_name', 'user_position', 'region_name', 'user_phone_number1')


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ('id', 'title', 'body',
                  'photo', 'news_date')


class CustomTokenSerializer(TokenSerializer):
    user = UserSerializer(read_only=True)

    class Meta(TokenSerializer.Meta):
        fields = ('key', 'user')


class FAQSerializer(serializers.ModelSerializer):
    class Meta:
        model = Faq
        fields = ('id', 'faq_name', 'faq_phone',
                  'faq_question', 'faq_answer', 'faq_status')