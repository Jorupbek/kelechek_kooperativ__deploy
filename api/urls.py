from rest_framework.routers import SimpleRouter

from api.views import NewsList, UserList, FAQList

router = SimpleRouter()

router.register('news', NewsList, 'news')
router.register('users', UserList, 'users')
router.register('faq', FAQList, 'faqs')

urlpatterns = router.urls
