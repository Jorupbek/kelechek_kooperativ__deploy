from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from agreement.models import UserAgreement


@admin.register(UserAgreement)
class UserAgreementAdmin(ImportExportModelAdmin):
    pass
