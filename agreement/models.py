from django.db import models


BOOKKEEPER_CHOICE = [
    ('Первоначальный взнос', 'Первоначальный взнос'),
    ('Пополнение', 'Пополнение'),
    ('Вступительный взнос', 'Вступительный взнос'),
    ('Погашение ссуды', 'Погашение ссуды'),
]


class UserAgreement(models.Model):
    agreement_city = models.CharField('Город договора', max_length=255,
                                      help_text='пример: г. Бишкек',
                                      null=True, blank=True)
    agreement_by_whom = models.TextField('В лице от кооператива',
                                         null=True, blank=True)
    agreement_date = models.DateField('Дата Договора об участии',
                                      null=True, blank=True)
    agreement_document_number = models.IntegerField('№ Договора об участии',
                                                    null=True, blank=True)
    agreement_amount = models.CharField('ссуда на сумму', max_length=255,
                                        help_text="787 500",
                                        null=True, blank=True)
    agreement_amount_text = models.CharField('ссуда на сумму в виде текста',
                                             null=True, blank=True,
                                             help_text="Семьсот восемьдесят семь тысяч пятьсот",
                                             max_length=255)
    agreement_month = models.IntegerField('Месяцев на ссуду',
                                          help_text='120', default=0,
                                          null=True, blank=True)
    agreement_month_text = models.CharField('Месяцев на ссуду в виде текста',
                                            help_text='сто двадцать',
                                            max_length=255,
                                            null=True, blank=True)
    agreement_home = models.TextField('Залоговое имущество',
                                      null=True, blank=True,
                                      help_text='''Квартира, полезной площадью 34,1 кв.м., 
                                    жилой площадью 18,2 кв.м., находящийся по адресу''')
    agreement_purchase = models.CharField('Договор купли-продажи №',
                                          max_length=255,
                                          null=True, blank=True)
    agreement_purchase_date = models.DateField('Дата Договора купли-продажи',
                                               null=True, blank=True)
    agreement_chairman = models.CharField(verbose_name='В лице Председателя',
                                          max_length=255, null=True,
                                          blank=True,
                                          help_text='в лице Председателя Правления Курманалиева Эламана Курманалиевича')
    agreement_chairman2 = models.CharField(verbose_name='В лице Председателя',
                                           max_length=255, null=True,
                                           blank=True,
                                           help_text='''в лице Председателя Правления 
                                    Курманалиева Эламана Курманалиевича''')
    home_price = models.IntegerField(default=0,
                                     verbose_name='Желаемая стоимость',
                                     help_text='Желаемая стоимость недвижимого имущества')
    home_month_price = models.IntegerField(default=0,
                                           help_text='Член кооператива оплачивает сумму в размере',
                                           verbose_name='Оплачиваемся сумма')
    home_price_text = models.CharField(max_length=255,
                                       verbose_name='Желаемая стоимость в письменном виде',
                                       help_text='''Желаемая стоимость недвижимого 
                                    имущества в письменном виде (два миллиона сомов)''')
    home_price_25percent_text = models.CharField(max_length=255,
                                                 verbose_name='25% от общей суммы в письменном виде',
                                                 help_text='25% от общей суммы в письменном виде')
    home_price_4percent_text = models.CharField(max_length=255,
                                                verbose_name='4% от общей суммы в письменном виде',
                                                help_text='4% от общей суммы в письменном виде')
    loan_amount_text = models.CharField('Оплачиваемая сумма в письменном виде',
                                        max_length=255, null=True, blank=True)
    prilojenie_date = models.DateField('Дата приложения',
                                       null=True, blank=True)
    prilojenie_count = models.IntegerField('Количество месяцев', default=0,
                                           null=True, blank=True)
    prilojenie_count_text = models.CharField('Количество месяцев текст',
                                             max_length=255, null=True,
                                             blank=True,
                                             help_text='''Текст количество 
                                             месяцев (6 месяцев)''')
    anketa_date_register = models.DateField('Дата регистрации анкеты',
                                            null=True, blank=True)
    contract_date = models.DateField('Дата договора ссуды',
                                     null=True, blank=True)
    contract_city = models.CharField('Город договора ссуды',
                                     help_text='г. Бишкек',
                                     max_length=255, null=True, blank=True)
    deposit_date = models.DateField('Дата договора о залоге',
                                    null=True, blank=True)
    deposit_date_text = models.CharField(
        'Дата договора о залоге в письменном виде',
        max_length=255, null=True, blank=True,
        help_text="""
                                         Двадцатое сентрября две тысячри двадцать второго года""")
    application_date = models.DateField('Дата приложения',
                                        null=True, blank=True)
    law_district = models.CharField('Регион нотариуса',
                                    max_length=255, null=True, blank=True)
    bookkeeper_choice = models.CharField(max_length=255,
                                         choices=BOOKKEEPER_CHOICE, null=True,
                                         blank=True,
                                         default='Первоначальный взнос',
                                         verbose_name="Выбор взноса")
    deposite_by = models.CharField(max_length=255, null=True, blank=True,
                                   verbose_name="По дов")
    law_payment = models.IntegerField('юридическое сопровождение', default=0,
                                      null=True, blank=True)
    law_payment_text = models.CharField('юридическое сопровождение текст',
                                        max_length=255, null=True, blank=True)
    percent_25_field = models.IntegerField("25%", null=True, blank=True,
                                           default=25)
    percent_25_field_text = models.CharField(
        '25% в виде текст (двадцвать пять процентов)',
        max_length=255, null=True, blank=True)
    percent_4_field = models.IntegerField("4%", null=True, blank=True,
                                          default=4)
    percent_4_field_text = models.CharField(
        '4%  в виде текст (четыре процента)',
        max_length=255, null=True, blank=True)

    def __str__(self):
        return f'{self.id}'

    def percent_25(self):
        percent = self.percent_25_field / 100
        return int(self.home_price * percent)

    def percent_4(self):
        percent = self.percent_4_field / 100
        return int(self.home_price * percent)