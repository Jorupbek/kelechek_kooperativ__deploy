from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import SelectMultiple

from protocols.models import UserProtocol
from users.models import CustomUser


class ProtocolCreateForm(forms.ModelForm):
    users_presented = forms.ModelMultipleChoiceField(
        label='Список присутствовавших',
        queryset=CustomUser.objects.filter(
            user_is_operator=False, is_staff=False),
        required=False,
        widget=SelectMultiple(
            attrs={'class': 'form-control'}
        ))

    users_accepted = forms.ModelMultipleChoiceField(
        label='Список принявших решение',
        queryset=CustomUser.objects.filter(
            user_is_operator=False, is_staff=False),
        required=False,
        widget=SelectMultiple(
            attrs={'class': 'form-control'}
        ))

    class Meta(UserCreationForm.Meta):
        model = UserProtocol
        fields = ('users_presented', 'users_accepted', 'protocol_city', 'protocol_date', 'protocol_question',
                  'protocol_decision', 'protocol_chairman')