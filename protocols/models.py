from django.db import models

from users.models import CustomUser


class UserProtocol(models.Model):
    protocol_created_user = models.ForeignKey(CustomUser,
                                              related_name='user_protocol',
                                              on_delete=models.CASCADE)
    protocol_city = models.CharField(
        'Город в протоколе', help_text="г. Бишкек", max_length=255, null=True,
        blank=True)
    protocol_date = models.DateField('Дата протокола', null=True, blank=True)
    protocol_question = models.TextField('Рассмотрели вопросы', null=True,
                                         blank=True,
                                         help_text='РАССМАТРИВАЕМЫЙ ВОПРОС')
    protocol_decision = models.TextField('Приняли решение', null=True,
                                         blank=True,
                                         help_text='РАССМОТРЕЛ ВОПРОС И РЕШИЛ')
    protocol_chairman = models.TextField('Председатель Правление Кооператива',
                                         null=True, blank=True, )

    def __str__(self):
        return f'{self.id}'
