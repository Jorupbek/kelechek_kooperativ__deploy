from django.urls import path

from protocols.views import (ProtocolUpdateView, ProtocolDetailView,
                             ProtocolCreateView, ProtocolListView)

urlpatterns = [
    path('protocol-update/<int:pk>/', ProtocolUpdateView.as_view(), name='protocol_update'),
    path('protocol-detail/<int:pk>/', ProtocolDetailView.as_view(), name='protocol_detail'),
    path('protocol-create/', ProtocolCreateView.as_view(), name='protocol_create'),
    path('protocol-list/', ProtocolListView.as_view(), name='protocol_list'),
]