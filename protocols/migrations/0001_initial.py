# Generated by Django 3.0 on 2022-01-09 20:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    state_operations = [
        migrations.CreateModel(
            name='UserProtocol',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('protocol_city', models.CharField(blank=True, help_text='г. Бишкек', max_length=255, null=True, verbose_name='Город в протоколе')),
                ('protocol_date', models.DateField(blank=True, null=True, verbose_name='Дата протокола')),
                ('protocol_question', models.TextField(blank=True, help_text='РАССМАТРИВАЕМЫЙ ВОПРОС', null=True, verbose_name='Рассмотрели вопросы')),
                ('protocol_decision', models.TextField(blank=True, help_text='РАССМОТРЕЛ ВОПРОС И РЕШИЛ', null=True, verbose_name='Приняли решение')),
                ('protocol_chairman', models.TextField(blank=True, null=True, verbose_name='Председатель Правление Кооператива')),
                ('protocol_created_user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_protocol', to=settings.AUTH_USER_MODEL)),
            ],
            bases=(models.Model,),
        ),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(state_operations=state_operations)
    ]
