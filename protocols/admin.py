from django.contrib import admin

# Register your models here.
from protocols.models import UserProtocol


class UserProtocolAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'protocol_city', 'protocol_created_user', 'protocol_date')
    list_display_links = ('id', 'protocol_city', 'protocol_date')


admin.site.register(UserProtocol, UserProtocolAdmin)