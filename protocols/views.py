from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, DetailView, UpdateView

from protocols.forms import ProtocolCreateForm
from protocols.models import UserProtocol


class ProtocolListView(UserPassesTestMixin, ListView):
    model = UserProtocol
    template_name = 'protocols/protocol_list.html'
    paginate_by = 30

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator

    def get_queryset(self):
        object_list = UserProtocol.objects.all().order_by('-id')

        return object_list


class ProtocolCreateView(UserPassesTestMixin, CreateView):
    model = UserProtocol
    template_name = 'protocols/protocol_create.html'
    form_class = ProtocolCreateForm
    success_url = reverse_lazy('protocol_list')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator

    def form_valid(self, form):
        form.instance.protocol_created_user = self.request.user
        return super().form_valid(form)


class ProtocolDetailView(UserPassesTestMixin, DetailView):
    model = UserProtocol
    template_name = 'protocols/protocol.html'

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator


class ProtocolUpdateView(UserPassesTestMixin, UpdateView):
    model = UserProtocol
    template_name = 'protocols/protocol_update.html'
    form_class = ProtocolCreateForm
    success_url = reverse_lazy('protocol_list')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator