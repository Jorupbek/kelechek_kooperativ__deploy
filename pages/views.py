from datetime import datetime

import xlwt
from django.contrib.auth.mixins import UserPassesTestMixin
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView
from xlwt import Borders, Alignment, Style

from faq.forms import FaqForm
from faq.models import Faq
from users.models import CustomUser


class HomePageView(TemplateView):
    template_name = 'index.html'


class AboutPageView(TemplateView):
    template_name = 'pages/about.html'


class ContactPageView(CreateView):
    model = Faq
    form_class = FaqForm
    template_name = 'pages/contact.html'
    success_url = reverse_lazy('home')


class AdminPageView(UserPassesTestMixin, TemplateView):
    template_name = 'pages/admin_page.html'

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.user_is_operator \
            or self.request.user.is_moderator


class CriteriaPageView(TemplateView):
    template_name = 'pages/criteria.html'


class ConditionsPageView(TemplateView):
    template_name = 'pages/conditions.html'


class StepsPageView(TemplateView):
    template_name = 'pages/steps.html'


class AdvantagesPageView(TemplateView):
    template_name = 'pages/advantages.html'


class RegulationPageView(TemplateView):
    def get(self, request, *args, **kwargs):
        with open('static/regulation.pdf', 'rb') as pdf:
            response = HttpResponse(pdf.read(), content_type='application/pdf')
            response['Content-Disposition'] = 'inline;filename=regulation.pdf'
            return response


def clm_style(height, border=True):
    font = xlwt.XFStyle()
    font.font.bold = True
    font.font.height = height
    font.alignment.horz = Alignment.HORZ_CENTER
    font.alignment.vert = Alignment.VERT_CENTER
    font.font.colour_index = Style.colour_map['black']
    if border:
        font.borders.top = Borders.THIN
        font.borders.right = Borders.THIN
        font.borders.bottom = Borders.THIN
        font.borders.left = Borders.THIN
    return font


def export_to_csv(request):
    title = 'Список участников'
    curDate = datetime.now().strftime("%d-%m-%Y")

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = (
        f'attachment; filename=\"Kelechek-kooperativ-{curDate}.xls\"')

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(title)

    columns = ['ФИО', 'Позиция', 'Баланс', 'Телефон', 'ИНН', 'Дата регистрации']

    row_num = 0

    ws.col(0).width = 25 * 600
    ws.col(1).width = 25 * 150
    ws.col(2).width = 25 * 270
    ws.col(3).width = 25 * 270
    ws.row(row_num).height = 400

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], clm_style(200))

    page_url = request.headers['Referer']
    filter = {
        'is_staff': False,
        'user_is_operator': False,
        'is_superuser': False,
        'user_is_queued': False if 'given_list' in page_url else True,
    }
    date_from = request.GET.get('date_from')
    date_to = request.GET.get('date_to')
    if date_from:
        filter['user_registration_date__gte'] = date_from
    if date_to:
        filter['user_registration_date__lte'] = date_to
    rows = (CustomUser.objects
            .filter(**filter)
            .order_by('user_position')
            .values_list('user_full_name',
                         'user_position',
                         'user_balance',
                         'user_phone_number1',
                         'user_inn',
                         'user_registration_date'))

    rows = [[x.strftime("%d-%m-%Y") if isinstance(x, datetime) else x for x in row] for row in rows]
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num])
    row_num += 2
    wb.save(response)

    return response


def error_404_view(request, exception):
    # we add the path to the the 404.html file
    # here. The name of our HTML file is 404.html
    return render(request, 'errors/404.html')
