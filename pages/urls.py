from django.urls import path

from pages.views import (
    AboutPageView, ContactPageView, AdminPageView, CriteriaPageView,
    ConditionsPageView, AdvantagesPageView, StepsPageView,
    export_to_csv, HomePageView, RegulationPageView
)

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('about/', AboutPageView.as_view(), name='about'),
    path('contact/', ContactPageView.as_view(), name='contact'),
    path('admin_page/', AdminPageView.as_view(), name='admin_page'),
    path('criteria/', CriteriaPageView.as_view(), name='criteria'),
    path('conditions/', ConditionsPageView.as_view(), name='conditions'),
    path('advantages/', AdvantagesPageView.as_view(), name='advantages'),
    path('steps/', StepsPageView.as_view(), name='steps'),
    path('regulation/', RegulationPageView.as_view(), name='regulation'),
    path('export-csv-file', export_to_csv, name='export_csv'),
]
