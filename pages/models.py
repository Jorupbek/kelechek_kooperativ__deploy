from django.db import models


class SiteInfo(models.Model):
    address = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    phone_contact1 = models.CharField(max_length=255, blank=True, null=True)
    phone_contact2 = models.CharField(max_length=255, blank=True, null=True)
    phone_contact3 = models.CharField(max_length=255, blank=True, null=True)
    work = models.CharField(max_length=255)
    email = models.CharField(max_length=255)