from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

from kelechek_project import settings

urlpatterns = [
    path('upr/admin/', admin.site.urls),
    path('captcha/', include('captcha.urls')),
    path('tinymce/', include('tinymce.urls')),
    path('', include('users.urls')),
    path('users/', include('django.contrib.auth.urls')),
    path('api/kelechek/', include('api.urls')),
    path('api/kelechek/rest-auth/', include('dj_rest_auth.urls')),
    path('', include('pages.urls')),
    path('', include('news.urls')),
    path('', include('faq.urls')),
    path('', include('ads.urls')),
    path('votes/', include('quotes.urls')),
    path('protocols/', include('protocols.urls')),
]
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATICFILES_DIRS)
    # urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

handler404 = 'pages.views.error_404_view'