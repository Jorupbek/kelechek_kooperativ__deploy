from django.db import models
from _datetime import datetime
from embed_video.fields import EmbedVideoField
from users.models import CustomUser


class News(models.Model):
    photo = models.ImageField(upload_to='images/', default='images/def.png',
                              verbose_name='Изображение', null=True,
                              blank=True)
    video = EmbedVideoField(verbose_name="ссылка на ютуб", null=True, blank=True, help_text = 'Ссылку только с ютуба')
    title = models.CharField(max_length=255, verbose_name='Заголовок')
    body = models.TextField(verbose_name='Описание')
    news_date = models.DateTimeField(default=datetime.now)
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE,
                               related_name='news_author')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


