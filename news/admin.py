from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from news.models import News


class NewsAdmin(ImportExportModelAdmin):
    list_display = ('title', 'news_date', )
    list_display_links = ('title', 'news_date', )


admin.site.register(News, NewsAdmin)
