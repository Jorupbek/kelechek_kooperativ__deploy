from django.urls import path
from .views import (NewsListPage, NewsCreateView,
                    NewsDetail, NewsDeleteView, NewsUpdateView, NewsReceivedListView)

urlpatterns = [
    path('news/', NewsListPage.as_view(), name='news'),
    path('news_received/', NewsReceivedListView.as_view(), name='news_received'),
    path('news/news_create/', NewsCreateView.as_view(), name='news_create'),
    path('news/<int:pk>/', NewsDetail.as_view(), name='news_detail'),
    path('news/<int:pk>/delete', NewsDeleteView.as_view(), name='news_delete'),
    path('news/<int:pk>/update', NewsUpdateView.as_view(), name='news_update'),
]
