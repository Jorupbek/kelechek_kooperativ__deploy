from django import forms
from .models import News


class NewsForm(forms.ModelForm):
    title = forms.CharField(label='Заголовок', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    body = forms.CharField(label='Описание', required=True, widget=forms.Textarea(attrs={'class': 'form-control'}))
    photo = forms.ImageField(label='Фото', required=True, widget=forms.FileInput(attrs={'class': 'form-control'}))

    class Meta:
        model = News
        fields = ['title', 'body', 'photo']
