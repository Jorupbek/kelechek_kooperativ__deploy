from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DeleteView, CreateView, DetailView, UpdateView

from news.forms import NewsForm
from news.models import News


class NewsListPage(ListView):
    queryset = News.objects.order_by('-news_date')
    template_name = 'news/news.html'
    paginate_by = 10
    success_url = reverse_lazy('news')


class NewsReceivedListView(UserPassesTestMixin, ListView):
    model = News
    template_name = 'news/news_received_list.html'
    context_object_name = 'received_news'

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator


class NewsCreateView(UserPassesTestMixin, CreateView):
    model = News
    form_class = NewsForm
    template_name = 'news/news_create.html'
    success_url = reverse_lazy('news_received')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(NewsCreateView, self).form_valid(form)

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator


class NewsDetail(DetailView):
    model = News
    template_name = 'news/news_detail.html'


class NewsDeleteView(UserPassesTestMixin, DeleteView):
    model = News
    template_name = 'news/news_delete.html'
    success_url = reverse_lazy('news_received')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator


class NewsUpdateView(UserPassesTestMixin, UpdateView):
    model = News
    template_name = 'news/news_update.html'
    fields = ['title', 'body', 'photo']
    success_url = reverse_lazy('news_received')

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_moderator
