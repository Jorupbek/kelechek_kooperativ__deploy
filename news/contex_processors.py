from .models import News


def last_news(request):
    news_context = News.objects.order_by('-id')[:3]
    return {'last_news': news_context}
